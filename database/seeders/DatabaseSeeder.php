<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\PermissionCategory;
use App\Models\Permission;
use App\Models\RolePermission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{

    protected $toTruncate = ['users', 'roles', 'permission_categories', 'permissions', 'role_permissions'];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //empty tables before seeding
        Model::unguard();

        Schema::disableForeignKeyConstraints();

        foreach ($this->toTruncate as $table) {
            DB::table($table)->truncate();
        }

        Schema::enableForeignKeyConstraints();

        $role = Role::create(['name' => 'superadmin']);

        $user = User::factory()->create([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'super@admin.com',
            'password' => 'secret',
            'role_id' => $role->id,
        ]);


        $roleTenant = Role::create(['name' => 'Tenants (Mobile Users)', 'created_by' => $user->id]);
        $user = User::factory()->create([
            'first_name' => 'Tenant',
            'last_name' => 'User',
            'email' => 'tenant@user.com',
            'password' => 'tenant',
            'role_id' => $roleTenant->id,
        ]);

        $roleManager = Role::create(['name' => 'Manager', 'created_by' => $user->id]);
        $user = User::factory()->create([
            'first_name' => 'Manager',
            'last_name' => 'User',
            'email' => 'manager@user.com',
            'password' => 'manager',
            'role_id' => $roleManager->id,
        ]);

        $permissionCategory = PermissionCategory::create([
            'name' => 'profile management',
            'created_by' => $user->id,
        ]);
        $permission = Permission::create([
            'name' => 'manage.profile',
            'permission_category_id' => $permissionCategory->id,
            'created_by' => $user->id,
        ]);

        RolePermission::factory()->create([
            'permission_id' => $permission->id,
            'role_id' => $role->id,
            'created_by' => $user->id,
        ]);

    }
}
