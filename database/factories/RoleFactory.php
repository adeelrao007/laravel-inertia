<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class RoleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $roles = ['superadmin', 'admin', 'editor', 'maintainer'];

        $users = User::all()->pluck('id')->toArray();
        return [
            'name' => $roles[rand(0,3)],
            'created_by' => $this->faker->randomElement($users),
        ];
    }
}
