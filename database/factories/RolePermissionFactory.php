<?php

namespace Database\Factories;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RolePermissionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $permissions = Permission::all()->pluck('id')->toArray();
        $roles = Role::all()->pluck('id')->toArray();
        return [
            'permission_id' => $this->faker->randomElement($permissions),
            'role_id' => $this->faker->randomElement($roles),
        ];
    }
}
