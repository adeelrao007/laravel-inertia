<?php

use App\Http\Controllers\AirQualityController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\BillPaymentsController;
use App\Http\Controllers\CivicAlertsController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\HomesController;
use App\Http\Controllers\MarketplaceController;
use App\Http\Controllers\PublicTransportController;
use App\Http\Controllers\RegisterPlatesController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\SportsActivitiesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Route::get('login', [AuthenticatedSessionController::class, 'create'])
    ->name('login')
    ->middleware('guest');
Route::post('login', [AuthenticatedSessionController::class, 'store'])
    ->name('login.store')
    ->middleware('guest');
Route::delete('logout', [AuthenticatedSessionController::class, 'destroy'])
    ->name('logout');
Route::get('language/{locale}', function ($locale) {
    Session()->put('locale', $locale);
    return redirect()->back();
})->name('language');


// Images
Route::get('/img/{path}', [ImagesController::class, 'show'])
    ->where('path', '.*')
    ->name('image');

Route::group(['middleware' => 'auth'], function () {

    // Users
    Route::get('/',  [UsersController::class, 'index'])
        ->name('users');
    Route::get('users', [UsersController::class, 'index'])
        ->name('users');
    Route::get('users/create', [UsersController::class, 'create'])
        ->name('users.create');
    Route::post('users', [UsersController::class, 'store'])
        ->name('users.store');
    Route::get('users/{user}/edit', [UsersController::class, 'edit'])
        ->name('users.edit');
    Route::put('users/{user}', [UsersController::class, 'update'])
        ->name('users.update');
    Route::delete('users/{user}', [UsersController::class, 'destroy'])
        ->name('users.destroy');
    Route::put('users/{user}/restore', [UsersController::class, 'restore'])
        ->name('users.restore');

    // Homes
    Route::get('homes', [HomesController::class, 'index'])
        ->name('homes');
    Route::get('homes/create', [HomesController::class, 'create'])
        ->name('homes.create');
    Route::post('homes', [HomesController::class, 'store'])
        ->name('homes.store');
    Route::get('homes/{home}/edit', [HomesController::class, 'edit'])
        ->name('homes.edit');
    Route::put('homes/{home}', [HomesController::class, 'update'])
        ->name('homes.update');
    Route::delete('homes/{home}', [HomesController::class, 'destroy'])
        ->name('homes.destroy');
    Route::put('homes/{home}/restore', [HomesController::class, 'restore'])
        ->name('homes.restore');
    Route::get('assign/home/{user}', [HomesController::class, 'userHomes'])
        ->name('assign.homes');
    Route::post('assign/home/{user}', [HomesController::class, 'assignHomeToUser'])
        ->name('assign.homes.user');

    // Homes Status
    Route::get('homes/create/status', [HomesController::class, 'createStatus'])
        ->name('homes.create.status');
    Route::post('homes/status', [HomesController::class, 'storeStatus'])
        ->name('homes.store.status');
    Route::get('homes/status/{homeStatus}/edit', [HomesController::class, 'editStatus'])
        ->name('homes.edit.status');
    Route::put('homes/status/{homeStatus}', [HomesController::class, 'updateStatus'])
        ->name('homes.update.status');
    Route::delete('homes/status/{homeStatus}', [HomesController::class, 'destroyStatus'])
        ->name('homes.destroy.status');
    Route::put('homes/status/{homeStatus}/restore', [HomesController::class, 'restoreStatus'])
        ->name('homes.restore.status');

    //Register Plates
    Route::resource('register-plates', RegisterPlatesController::class);
    //Public Transport
    Route::resource('public-transport', PublicTransportController::class);
    //Sports Activities
    Route::resource('sports-activities', SportsActivitiesController::class);
    //Bill Payments
    Route::resource('bill-payments', BillPaymentsController::class);
    //Civiv Alerts
    Route::resource('civic-alerts', CivicAlertsController::class);
    //Services
    Route::resource('services', ServicesController::class);
    //Air Quality
    Route::resource('air-quality', AirQualityController::class);
    //Marketplace
    Route::resource('marketplace', MarketplaceController::class);
});
