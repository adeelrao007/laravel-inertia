<?php

namespace App\Http\Controllers;

use App\Models\Home;
use App\Models\HomeStatus;
use App\Models\User;
use App\Models\UserHome;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;

class HomesController extends Controller
{

    protected $homeTab = 'homes';
    protected $constructionStatusTab = 'construction_statuses';

    public function __construct()
    {
        $locale = Session()->get('locale') ?? app()->getLocale();

        app()->setLocale($locale);
    }

    public function index()
    {
        return Inertia::render('Homes/Index', [
            'filters' => Request::all('search', 'role', 'trashed'),
            'mySelectedTab' => Request::get('select_tab') ?? 'homes',
            'homes' => Home::orderByName()
                ->get()
                ->transform(fn (Home $home) => [
                    'id' => $home->id,
                    'name' => $home->name,
                    'status_name' => $home->homeStatus->name,
                    'completion_date' => $home->completion_date,
                    'deleted_at' => $home->deleted_at,
                ]),
            'statuses' => HomeStatus::orderByName()
                ->get()
                ->transform(fn (HomeStatus $status) => [
                    'id' => $status->id,
                    'name' => $status->name,
                    'deleted_at' => $status->deleted_at,
                ]),
        ]);
    }

    public function userHomes(User $user){
        return Inertia::render('Homes/UserHome', [
            'userHomes' => UserHome::getHomes($user->id)
                ->get()
                ->transform(fn (UserHome $userHome) => [
                    'id' => $userHome->id,
                    'user_name' => $userHome->user->getNameAttribute,
                    'home_name' => $userHome->home->name,
                ]),
                'user' => $user->id,
                'homes' => Home::orderByName()
                    ->whereNotIn('id', UserHome::getHomes($user->id)->pluck('id'))
                    ->get()
                    ->transform(fn (Home $home) => [
                        'id' => $home->id,
                        'name' => $home->name,
                    ]),
        ]);
    }

    public function assignHomeToUser(User $user){
        Request::validate([
            'home_id' => ['required'],
        ]);

        UserHome::create([
            'user_id' => $user->id,
            'home_id' => Request::get('home_id'),
        ]);
    }

    public function create()
    {
        return Inertia::render('Homes/Create', [
            'homeStatuses' => HomeStatus::orderByName()
                ->get()
                ->transform(fn (HomeStatus $status) => [
                    'id' => $status->id,
                    'name' => $status->name,
                ]),
        ]);
    }

    public function createStatus()
    {
        return Inertia::render('Homes/CreateStatus');
    }

    public function store()
    {
        Request::validate([
            'name' => ['required', 'max:50'],
            'completion_date' => ['required', 'date'],
            'home_status_id' => ['required'],
        ]);

        Home::create([
            'name' => Request::get('name'),
            'completion_date' => Request::get('completion_date'),
            'home_status_id' => Request::get('home_status_id'),
        ]);

        return Redirect::back()->with('success', trans('homes.home_created'));
    }

    public function storeStatus()
    {
        Request::validate([
            'name' => ['required', 'max:50'],
        ]);

        HomeStatus::create([
            'name' => Request::get('name'),
        ]);

        return Redirect::back()->with('success', trans('homes.status_created'));
    }

    public function edit(Home $home)
    {
        return Inertia::render('Homes/Edit', [
            'home' => [
                'id' => $home->id,
                'name' => $home->name,
                'completion_date' => $home->completion_date,
                'home_status_id' => $home->home_status_id,
                'deleted_at' => $home->deleted_at,
            ],
            'homeStatuses' => HomeStatus::orderByName()
                ->get()
                ->transform(fn (HomeStatus $status) => [
                    'id' => $status->id,
                    'name' => $status->name,
                ]),
        ]);
    }

    public function editStatus(HomeStatus $homeStatus)
    {
        return Inertia::render('Homes/EditStatus', [
            'homeStatus' => [
                'id' => $homeStatus->id,
                'name' => $homeStatus->name,
                'completion_date' => $homeStatus->completion_date,
                'deleted_at' => $homeStatus->deleted_at,
            ],
        ]);
    }

    public function update(Home $home)
    {
        $home->update(
            Request::validate([
                'name' => ['required', 'max:50'],
                'completion_date' => ['required', 'date'],
                'home_status_id' => ['required'],
            ])
        );

        return Redirect::back()->with('success', trans('homes.home_updated'));
    }

    public function updateStatus(HomeStatus $homeStatus)
    {
        $homeStatus->update(
            Request::validate([
                'name' => ['required', 'max:50'],
            ])
        );

        return Redirect::back()->with('success', trans('homes.status_updated'));
    }

    public function destroy(Home $home)
    {
        $home->delete();

        return Redirect::back()->with('success', trans('homes.home_deleted'));
    }

    public function destroyStatus(HomeStatus $homeStatus)
    {
        $homeStatus->delete();

        return Redirect::back()->with('success', trans('homes.status_deleted'));
    }

    public function restore(Home $home)
    {
        $home->restore();

        return Redirect::back()->with('success', trans('homes.home_restored'));
    }

    public function restoreStatus(HomeStatus $homeStatus)
    {
        $homeStatus->restore();

        return Redirect::back()->with('success', trans('homes.status_restored'));
    }
}
