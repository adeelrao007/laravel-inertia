<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\PermissionCategory;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class UsersController extends Controller
{

    public function __construct()
    {
        $locale = Session()->get('locale') ?? app()->getLocale();

        app()->setLocale($locale);
    }

    public function index()
    {
        return Inertia::render('Users/Index', [
            'filters' => Request::all('search', 'role', 'trashed'),
            'users' => User::orderByName()
                ->where('id', '!=', 1)
                ->filter(Request::only('search', 'role', 'trashed'))
                ->get()
                ->transform(fn (User $user) => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'role_name' => $user->role->name,
                    'is_tenant' => ($user->role_id === 2),
                    'deleted_at' => $user->deleted_at,
                ]),
            'permission_categories' => PermissionCategory::orderByName()
                ->get()
                ->transform(fn ($category) => [
                    'id' => $category->id,
                    'name' => $category->name,
                ]),
            'permissions' => Permission::orderByName()
                ->get()
                ->transform(fn ($permission) => [
                    'id' => $permission->id,
                    'name' => $permission->name,
                ]),
            'roles' => Role::orderByName()
                ->get()
                ->transform(fn ($role) => [
                    'id' => $role->id,
                    'name' => $role->name,
                ]),
        ]);
    }

    public function create()
    {
        return Inertia::render('Users/Create', [
            'roles' => Role::orderByName()->where('id', '!=', 1)
                ->get()
                ->transform(fn ($role) => [
                    'id' => $role->id,
                    'name' => $role->name,
                ]),
        ]);
    }

    public function store()
    {
        Request::validate([
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'email' => ['required', 'max:50', 'email', Rule::unique('users')],
            'role_id' => ['required'],
        ]);

        User::create([
            'first_name' => Request::get('first_name'),
            'last_name' => Request::get('last_name'),
            'email' => Request::get('email'),
            'role_id' => Request::get('role_id'),
        ]);

        return Redirect::route('users')->with('success', trans('users.user_created'));
    }

    public function edit(User $user)
    {

        $loggedInUser = Auth::user();
        return Inertia::render('Users/Edit', [
            'user' => [
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'role_id' => $user->role_id,
                'deleted_at' => $user->deleted_at,
            ],
            'isLoggedIn' => ($user->id === $loggedInUser->id),
            'roles' => Role::orderByName()->where('id', '!=', 1)
                ->get()
                ->transform(fn ($role) => [
                    'id' => $role->id,
                    'name' => $role->name,
                ]),
        ]);
    }

    public function update(User $user)
    {

        $loggedInUser = Auth::user();
        if ($user->isSuperUser() && $loggedInUser->id !== $user->id) {
            return Redirect::back()->with('error', trans('users.updating_super_user_is_not_allowed'));
        }

        $rules = [
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'email' => ['required', 'max:50', 'email', Rule::unique('users')->ignore($user->id)],
        ];

        $updateFields = array('first_name', 'last_name', 'email');

        if($loggedInUser->id !== $user->id){
            $rules['role_id'] = ['required'];
            $updateFields[] = 'role_id';
        }

        Request::validate($rules);

        $user->update(Request::only($updateFields));

        return Redirect::back()->with('success', trans('users.user_updated'));
    }

    public function destroy(User $user)
    {
        if ($user->isSuperUser()) {
            return Redirect::back()->with('error', trans('users.deleting_super_user_is_not_allowed'));
        }

        $user->delete();

        return Redirect::back()->with('success', trans('users.user_deleted'));
    }

    public function restore(User $user)
    {
        $user->restore();

        return Redirect::back()->with('success', trans('users.user_restored'));
    }
}
