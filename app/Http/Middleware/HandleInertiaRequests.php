<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Inertia\Middleware;
use App\Models\Permission;
use Illuminate\Support\Facades\File;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {

        $permissionList = array();
        $allPermissions = Permission::all()->pluck('name');
        foreach ($allPermissions as $permission) {
            $permissionList[str_replace('.', '_', $permission)] = false;
        }

        if ($request->user()) {
            $userTole = $request->user()->role;
            foreach ($userTole->rolePermission as $rolePermission) {
                $permissionList[str_replace('.', '_', $rolePermission->permission->name)] = true;
            }
        }

        $locale = Session()->get('locale') ?? app()->getLocale();

        app()->setLocale($locale);

        $path = resource_path('lang/' . $locale);
        $languageFiles = File::allFiles($path);
        $allTranslations = array();
        foreach($languageFiles as $file){
            $allTranslations[pathinfo($file, PATHINFO_FILENAME)] = File::getRequire($file);
        }

        return array_merge(parent::share($request, $locale, $allTranslations), [
            'auth' => function () use ($request, $permissionList) {
                return [
                    'user' => $request->user() ? [
                        'id' => $request->user()->id,
                        'first_name' => $request->user()->first_name,
                        'last_name' => $request->user()->last_name,
                        'email' => $request->user()->email,
                    ] : null,
                    'permissions'  => $permissionList,
                ];
            },
            'locale' => function () use ($locale) {
                return $locale;
            },
            'translations' => function () use ($allTranslations) {
                return $allTranslations;
            },
            'flash' => function () use ($request) {
                return [
                    'success' => $request->session()->get('success'),
                    'error' => $request->session()->get('error'),
                ];
            },
        ]);
    }
}
