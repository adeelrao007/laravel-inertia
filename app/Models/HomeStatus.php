<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HomeStatus extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function scopeOrderByName($query)
    {
        $query->orderBy('name');
    }

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where($field ?? 'id', $value)->withTrashed()->firstOrFail();
    }

    public function homes()
    {
        return $this->hasMany(Home::class);
    }
}
