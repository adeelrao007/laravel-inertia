<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PermissionCategory extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function scopeOrderByName($query)
    {
        $query->orderBy('name');
    }

    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }
}
