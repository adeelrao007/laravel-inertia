<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Home extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $casts = [
        'completion_date'  => 'immutable_datetime:Y-m-d\TH:i:s\Z',
    ];

    public function getCompletionDateAttribute($value){
        return Carbon::createFromFormat('Y-m-d', $value)->format('m/d/Y');
    }

    public function homeStatus(){
        return $this->belongsTo(HomeStatus::class);
    }

    public function setCompletionDateAttribute($value){
        $this->attributes['completion_date'] = Carbon::createFromFormat('Y-m-d\TH:i:s.uT', $value, 'UTC')->addDay()->format("Y-m-d");
    }

    public function scopeOrderByName($query)
    {
        $query->orderBy('name');
    }

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where($field ?? 'id', $value)->withTrashed()->firstOrFail();
    }
}
