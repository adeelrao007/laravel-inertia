<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class UserHome extends Model
{
    use HasFactory, SoftDeletes;

    public function user(){
        $this->belongsTo(User::class);
    }

    public function home(){
        $this->belongsTo(Home::class);
    }

    public function scopeWhereUser($query, $userId){
        $query->where('user_id', $userId);
    }

    public function scopeGetHomes($query, $userId = false){
        $query->when($userId ?? null, function ($query, $userId) {
            $query->where(function ($query) use ($userId) {
                $query->whereUser($userId);
            });
        });
    }

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where($field ?? 'id', $value)->withTrashed()->firstOrFail();
    }
}
