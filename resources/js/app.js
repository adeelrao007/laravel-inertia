import { createApp, h } from 'vue'
import { InertiaProgress } from '@inertiajs/progress'
import { createInertiaApp } from '@inertiajs/inertia-vue3'



InertiaProgress.init()

createInertiaApp({
    resolve: name => require(`./Pages/${name}`),
    title: title => `${title} - Liziera de Lac`,
    setup({ el, App, props, plugin }) {
        createApp({ render: () => h(App, props) })
            .use(plugin)
            .mixin({
                methods: {
                    $trans(key, allTranslations = this.$page.props.translations) {
                        let allKeys = [];
                        allKeys = key.split('.');
                        return (allKeys[2]) ? allTranslations[allKeys[0]][allKeys[1]][allKeys[2]] : allTranslations[allKeys[0]][allKeys[1]];
                    },
                }
            })
            .mount(el)
    },
})
