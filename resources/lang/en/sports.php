
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sports activities pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for sports activities pages
    | messages that we need to display to the user.
    |
    */

    'sports_activities' => 'Sports Activities',
];
