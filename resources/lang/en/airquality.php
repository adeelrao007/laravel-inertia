
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Air quality management pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for air quality management pages
    | messages that we need to display to the user.
    |
    */

    'air_quality' => 'Air Quality',
];
