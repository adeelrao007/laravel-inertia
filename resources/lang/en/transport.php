
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Public transport pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for public transport pages
    | messages that we need to display to the user.
    |
    */

    'public_transport' => 'Public Transport',
];
