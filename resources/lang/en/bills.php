
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bills management pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for bills management pages
    | messages that we need to display to the user.
    |
    */

    'bill_payments' => 'Bill Payments',
];
