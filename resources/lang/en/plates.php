<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Plates registration pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for plates registration pages
    | messages that we need to display to the user.
    |
    */

    'registered_plates' => 'Registered Plates',
];
