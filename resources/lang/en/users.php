<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Users pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for users pages
    | messages that we need to display to the user.
    |
    */

    'my_profile' => 'My Profile',
    'logout' => 'Logout',
    'users' => 'Users',
    'user' => 'User',
    'create' => 'Create',
    'homes' => 'Homes',
    'create_user' => 'Create User',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email' => 'Email',
    'role' => 'Role',
    'select_role' => 'Select Role',
    'user_created' => 'User Created',
    'updating_super_user_is_not_allowed' => 'Updating the super user is not allowed.',
    'user_updated' => 'User updated.',
    'deleting_super_user_is_not_allowed' => 'Deleting the super user is not allowed.',
    'user_restored' => 'User restored.',
    'user_deleted' => 'User deleted.',
    'this_user_has_been_deleted' => 'This user has been deleted.',
    'delete_user' => 'Delete User',
    'update_user' => 'Update User',
    'name' => 'Name',
    'no_users_found' => 'No users found.',
    'facility_reservations' => 'Facility Reservations',
    'bulleton_board' => 'Bulleton Board',
    'service_reservations' => 'Service Reservation',
    'are_your_sure_you_want_to_delete_this_user' => 'Are you sure you want to delete this user?',
    'are_your_sure_you_want_to_restore_this_user' => 'Are you sure you want to restore this user?',
];
