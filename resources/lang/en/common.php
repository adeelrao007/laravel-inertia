
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Commonly used Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for commonly used
    | messages that we need to display to the user.
    |
    */

    'created_successfuly' => 'Created Successfuly',
    'updated_successfuly' => 'Updated Successfuly',
    'deleted_successfuly' => 'Deleted Successfuly',
    'restored_successfuly' => 'Restored Successfuly',
];
