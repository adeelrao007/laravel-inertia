<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Login page Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for login page
    | messages that we need to display to the user.
    |
    */

    'welcome_back' => 'Welcome Back!',
    'email' => [
        'label' => 'Email',
        'enter' => 'enter your email',
    ],
    'password' => [
        'label' => 'Password',
        'enter' => 'enter your password',
    ],
    'remember_me' => 'Remember Me',
    'login' => 'Login',
];
