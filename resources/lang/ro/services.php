
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Service bookings pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for service bookings pages
    | messages that we need to display to the user.
    |
    */

    'services' => 'Services',
];
