<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Homes pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for homes pages
    | messages that we need to display to the user.
    |
    */

    'homes' => 'Homes',
    'home' => 'Home',
    'create' => 'Create',
    'homes' => 'Homes',
    'create_home' => 'Create Home',
    'home_created' => 'Home Created',
    'home_updated' => 'Home updated.',
    'home_restored' => 'Home restored.',
    'home_deleted' => 'Home deleted.',
    'this_home_has_been_deleted' => 'This home has been deleted.',
    'delete_home' => 'Delete Home',
    'update_home' => 'Update Home',
    'name' => 'Name',
    'no_homes_found' => 'No homes found.',
    'are_you_sure_you_want_to_delete_this_home' => 'Are you sure you want to delete this home?',
    'are_you_sure_you_want_to_restore_this_home' => 'Are you sure you want to restore this home?',
    'new_status' => 'New Status',
    'status_name' => 'Status Name',
    'no_statuses_found' => 'No statuses found',
    'create_status' => 'Create Construction Status',
    'status_created' => 'Construction Status Created',
    'status_updated' => 'Construction Status updated.',
    'status_restored' => 'Construction Status restored.',
    'status_deleted' => 'Construction Status deleted.',
    'this_status_has_been_deleted' => 'This Construction Status has been deleted.',
    'delete_status' => 'Delete Construction Status',
    'update_status' => 'Update Construction Status',
    'are_you_sure_you_want_to_delete_this_construction_status' => 'Are you sure you want to delete this construction status?',
    'are_you_sure_you_want_to_restore_this_construction_status' => 'Are you sure you want to restore this construction status?',
    'construction_statuses' => 'Construction Statuses',
    'construction_status' => 'Construction Status',
    'select_construction_status' => 'Select Construction Status',
    'expected_completion_date' => 'Expected Completion Date',
];
