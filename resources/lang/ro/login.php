<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Login page Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for login page
    | messages that we need to display to the user.
    |
    */

    'welcome_back' => 'Bine ai revenit!',
    'email' => [
        'label' => 'E-mail',
        'enter' => 'introduceți adresa dvs. de email',
    ],
    'password' => [
        'label' => 'Parola',
        'enter' => 'introduceți parola',
    ],
    'remember_me' => 'Ține-mă minte',
    'login' => 'Autentificare',
];
