
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Civic alerts pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for civic alerts pages
    | messages that we need to display to the user.
    |
    */

    'civic_alerts' => 'Civic Alerts',
];
