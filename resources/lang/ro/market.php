
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Marketplace management pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for marketplace management pages
    | messages that we need to display to the user.
    |
    */

    'marketplace' => 'Marketplace',
];
